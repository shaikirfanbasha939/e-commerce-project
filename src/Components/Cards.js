import React, { useState } from 'react'
import Products from './Products'
import { useNavigate } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import axios from 'axios'

function Cards(props) {
  const navigate = useNavigate()
  const handleClick = () => {
    navigate('/products')
  }

  const [product, setProduct] = useState({
    Name: "",
    Price: "",
    Img: ""

  })

  const handleInput = (e) => {
    const { name, value } = e.target
    setProduct({
      ...product,

      [name]: value
    })
  }
  console.log(product)

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.post('http://localhost:3001/register', product)
      console.log(response)
      alert("Register Successful")


    } catch (err) {
      throw err
    }

  }


  return (

    <div className='col-sm-6 @media (min-width: 576px) { ... }  col-md-4 col-lg-3 card border-0 mt-3' onClick={handleClick} onSubmit={handleSubmit}>
      <img src={props.data.Img} alt="images" className='w-100 rounded-3' onChange={handleInput} value={product.Img} name="Img" />
      <div className='card-body'>
        <p className='text-seconadry text-center' onChange={handleInput} value={props.data.Name} name="Name">{props.data.Name}</p>
        <p className='text-danger text-center' onChange={handleInput} value={props.data.Price} name="Price">{props.data.Price}</p>
      </div>
      <div className='footer text-center'> <Button className='btn btn-primary ' type='submit'>{props.data.Action}</Button></div>

    </div>

  )
}

export default Cards